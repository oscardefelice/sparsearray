# sparseArray

Module containing the objects to handle the counting of queries appearing in strings.
It solves [this problem](https://www.hackerrank.com/challenges/sparse-arrays/problem?isFullScreen=true), concerning the counting of the number of occurences of a given string in a list of strings.

## The problem

The issue can be schematised as follows.

There is a collection of input strings and a collection of query strings.
For each query string, one wants to determine how many times it occurs in the list of input strings.
For example, given input `string = ['a', 'maison', 'a', 'c']` and `queries = ['a', 'c', 'e']`, we find $2$ instances of `'a'`, $1$ of `'c'` and none of `'e'`. For each query, we add an element to our return array `results = [2,1,0]`.

### Installation instructions

To not mess up with versioning and package installation, we strongly advice to create a virtual environment.
One can follow [this guide](https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/) and the suitable section according to the OS.

Once the virtual environment has been set up, as usual, one has to run the following instruction from a command line

```bash
  pip install -r requirements.txt
```

This installs all the packages the code in this repository needs.

## The stringMatcher object

Making use of the principles of _object oriented programming_ (OOP), we modelised the problem above into an object we named `stringMatcher`.

The constructor takes two arguments, _i.e._ the two lists of strings.

The method `matchStrings` allows to perform the counting of each element of the queries in the strings list.

## The script

In `main.py` a script importing `SparseArray` module has been developed.

The default values of global variables has been set into a [yaml file](https://martin-thoma.com/configuration-files-in-python/#yaml). To change them or to run the script in different situation it is sufficient to just pass different yaml files, modelling the behaviour by the set of configuration values.

It has been set that the `queries` argument has to be passed by command line.
Indeed, to call the script execution it is sufficient to type
```bash
$: python -m main ab, bc, bb
```
to get the printed counting of the query elements in a dictionary, _e.g._
```bash
{ab: 1, bc: 0, bb: 2}
```
