# -*- coding: utf-8 -*-
###
### SparseArray.py
###
### Created by Oscar de Felice on 29/01/2020.
### Copyright © 2020 Oscar de Felice.
###
### <oscar.defelice@gmail.com>
###
### This program is free software: you can redistribute it and/or modify
### it under the terms of the GNU General Public License as published by
### the Free Software Foundation, either version 3 of the License, or
### (at your option) any later version.
###
### This program is distributed in the hope that it will be useful,
### but WITHOUT ANY WARRANTY; without even the implied warranty of
### MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
### GNU General Public License for more details.
###
### You should have received a copy of the GNU General Public License
### along with this program. If not, see <http://www.gnu.org/licenses/>.
###
########################################################################
###
### SparseArray.py
###
### Module containing the objects to handle the counting of queries appearing in strings.
###
### 29/01/2020 - Oscar: Creation of this module.
### 29/01/2020 - Oscar: Definition of stringMatcher class.
### 29/01/2020 - Oscar: Definition of custom Exception classes.
### 30/01/2020 - Oscar: Definition of __emptyNotAllowed method.
### 30/01/2020 - Oscar: Definition of __str__ magic method.
###

#!/bin/python3

"""
    Module containing the definition of stringMatcher and its methods.
"""

### exceptions classes ###
class Error(Exception):
    """
        Base class for other exceptions.
    """
    pass

class EmptyListError(Error):
    """
        Raised when the query or the string list do not contain any element.
    """
    pass

### stringMatcher class ###
class stringMatcher(object):
    """
        stringMatcher class.

        Object collecting methods to operate on two list of strings.
        It makes use of string methods to count how many times each element of the query list
        appears in the strings list.
        If lists are empty it raises an error.

        Parameters
        ----------
        strings         : list of strings.
                            It is the list in which we look for occurences of queries elements.

        queries         : list of strings.
                            It contains the strings we want to count the occurence of.
                            If it contains some doubled values, the constructor removes them.

        empty_allowed   : bool.
                            It triggers the behaviour of the class when empty lists are passed.
                            When True, empty lists are considered and the counting will return an empty list.
                            When False, if an empty list is passed, the constructor raises an Exception.
                            default : True

        Attributes
        ----------
        strings : list of strings.

        queries : list of strings.

        counted_queries_ : list of int.
                            Present only after calling the get_user_names method.
    """

    def __init__(self, strings, queries, empty_allowed = True):
        """
            Constructor method for the stringMatcher.
        """
        super(stringMatcher, self).__init__()

        if empty_allowed:
            self.strings_ = strings
            self.queries_ = list(set(queries)) # queries have to be unique.
                                               # Reconverted in list for aestethic purposes.
        else:
            self.__emptyNotAllowed(strings)
            self.strings_ = strings
            self.__emptyNotAllowed(queries)
            self.queries_ = list(set(queries))


    @staticmethod
    def __emptyNotAllowed(given_list):
        """
            private static method to raise exceptions
            if empty lists are not allowed.
        """
        if len(given_list) == 0:
            raise EmptyListError('Pass non-empty lists')

    def __todict(self):
        """
            private method to convert the counting list into
            a dictionary, whose keys are the elements of queries list.

            Return
            ------
            a dictionary with queries as keys and number of occurences as values.
        """
        queries = list(self.queries_)
        counting = self.counted_queries_
        return {query: count for query, count in zip(queries, counting)}

    def matchStrings(self):
        """
            matchStrings method.

            It performs the counting of occurences of each element of queries list
            in strings list.

            It updates the object itself with a new attribute counted_queries_.

            Returns
            -------
            the self object itself, with attribute counted_queries_ updated.
        """
        result = []
        for query in self.queries_:
            result.append(self.strings_.count(query))

        self.counted_queries_ = result
        return self

    def __str__(self):
        """
            magic method to set the print() behaviour of the object.

            Returns
            -------
                A string version of a dictionary counting how many times the
                queries elements appear in strings list if matchStrings method
                has been called before.
                Otherwise it returns the string version of the two lists.

        """
        if hasattr(self, 'counted_queries_'):
            return str(self.__todict())
        else:
            return 'Strings: ' + str(self.strings_) + '\n' + 'Queries: ' + str(self.queries_) + '\n'
