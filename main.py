## import libraries
import SparseArray as sa
import yaml
import os
import argparse

## assign the strings default as in the yaml file.
DEFAULT_VARIABLES = yaml.load(open('config_variables.yml'), Loader=yaml.FullLoader)

STRINGS = DEFAULT_VARIABLES['STRINGS'] # Default value of strings list.
EMPTY_ALLWD = DEFAULT_VARIABLES['EMPTY_BEHAVIOUR'] # Default value of empty list behaviour.

## parser to make the main accept command line arguments
parser = argparse.ArgumentParser()
parser.add_argument('queries', type=str, nargs='+', help= 'list of query strings')

## assign the queries variable to the argument read from command line.
queries = parser.parse_args().queries

## instantiate matcher object
matcher = sa.stringMatcher(strings = STRINGS, queries = queries, empty_allowed = EMPTY_ALLWD)

## perform the counting
matcher.matchStrings()

## print the result
print(matcher)
